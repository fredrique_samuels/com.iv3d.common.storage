package com.iv3d.common.storage;
/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

import org.springframework.util.FileCopyUtils;

import com.iv3d.common.Filter;
import com.iv3d.common.utils.JsonUtils;

public class FileSystemDao<T extends AuditedDataParam>{

	private final static String JSON_EXT = ".json";
	private final static String BIN_EXT = ".bin";

	private File path;
	private Class<T> clazz;

	public FileSystemDao(File dataRoot, String daoFolder, Class<T> clazz) {
		this.clazz = clazz;
		this.path = new File(dataRoot, daoFolder);
	}

	public final File getDataFolder() {
		return path;
	}

	public final void upsert(T auditedData) {
		path.mkdirs();
		keepCreationDate(auditedData);
		File file = getJsonFileById(auditedData.getId());
		JsonUtils.writeToFile(file, auditedData);
	}
	
	private void keepCreationDate(T auditedData) {
		T[] ts = get(auditedData.getId());
		if(ts.length!=0) {
			auditedData.setDateCreated(ts[0].getDateCreated());
		}
	}

	private File getJsonFileById(String id) {
		return getFileById(id, JSON_EXT);
	}

	public final T[] get(String ... ids) {
		Filter<T> filter = new Filter<T>() {

			@Override
			public boolean accept(T readFromFile) {
				return true;
			}
		};
		return get(filter, ids);
	}
	
	@SuppressWarnings("unchecked")
	public final T[] get(Filter<T> filter, String ... ids) {
		ArrayList<T> arrayList = new ArrayList<T>();
		
	    
		for(String id : ids) {
			File file = getJsonFileById(id);
			if(!file.exists()) {
				continue;
			}
			T readFromFile = JsonUtils.readFromFile(file, clazz);
			if(filter.accept(readFromFile)) {
				arrayList.add(readFromFile);
			}
		}
		
		return arrayList.toArray((T[]) Array.newInstance(clazz, arrayList.size()));
	}
	
	public final void upsertStream(String id, InputStream inputStream) {
		path.mkdirs();
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(getStreamFileById(id));
			FileCopyUtils.copy(inputStream, outputStream);
		} catch (FileNotFoundException e) {
			throw new DaoError(e);
		} catch (IOException e) {
			throw new DaoError(e);
		} finally {
			if(outputStream!=null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					//ignore
				}
			}
		}		
	}

	public InputStream getStreamById(String id) {
		File initialFile = getStreamFileById(id);
	    try {
			return new FileInputStream(initialFile);
		} catch (FileNotFoundException e) {
			throw new DaoError(e);
		}
	}

	protected File getStreamFileById(String id) {
		return getFileById(id, ".bin");
	}

	private File getFileById(String id, String ext) {
		return new File(path.getPath(), id+ext);
	}
	
	public static final class DaoError extends RuntimeException {
		private static final long serialVersionUID = 1L;
		
		public DaoError(Exception e) {
			super(e);
		}
	}

	public final String[] getIdsList() {
		return getIdsList(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(JSON_EXT) || name.endsWith(BIN_EXT);
			}
		});
	}
	
	protected final String[] getIdsList(final FilenameFilter filter) {
		final ArrayList<String> result = new ArrayList<String>(); 
		path.listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				if(filter.accept(dir, name)) {
					result.add(name.substring(0, name.length()-5));
				}
				return false;
			}
		});
		return result.toArray(new String[]{});
	}


	
}
